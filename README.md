# Hetzner Cloud Mobile

[Google Play Store](https://play.google.com/store/apps/details?id=de.lkdevelopment.hetzner)

## Lokales Testen & Entwickeln
Zum lokalen Test & Entwickeln wird benötigt die NPM & die Ionic CLI.
```bash
npm install -g ionic@latest
git clone https://github.com/LKDevelopment/hetzner-cloud-mobile-app.git
cd hetzner-cloud-mobile-app
npm install
ionic serve
```
Nun sollte sich der Standardbrowser öffnen und unter https://localhost:8100 erscheint die App nun.


## Sicherheit
Sollten Sie irgendwelche Sicherheitsfehler finden so wenden Sie sich doch bitte per E-Mail an apps@lk-development.de

## Lizenz
Die App wird als OpenSource unter der [MIT Lizenz](LICENSE.md) entwickelt.
